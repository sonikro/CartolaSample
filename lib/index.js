"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = require("node-fetch");
let requestInit;
requestInit = {
    method: 'GET',
    headers: {
        'X-GLB-Token': '1636a81d6c2ff74d276a47ffa4770d5ac32555552767372584d4b784e446f595f666c7356777958447548364c4f37595168304b6a4552706f69336a6b7374744a5364506e41724c6a6537336b307475683430427475374b77524e4b6f4b3663704c4b746f52513d3d3a303a6d7572696c6c6f6d616d7564'
    }
};
node_fetch_1.default('https://api.cartolafc.globo.com/auth/liga/letnis-champions-league', requestInit).then((response) => __awaiter(this, void 0, void 0, function* () { return console.log(yield response.json()); }), (error) => console.log(error));
//# sourceMappingURL=index.js.map