import fetch from 'node-fetch';

let requestInit: RequestInit;

requestInit = {
    method: 'GET',
    headers: {
        'X-GLB-Token': '1636a81d6c2ff74d276a47ffa4770d5ac32555552767372584d4b784e446f595f666c7356777958447548364c4f37595168304b6a4552706f69336a6b7374744a5364506e41724c6a6537336b307475683430427475374b77524e4b6f4b3663704c4b746f52513d3d3a303a6d7572696c6c6f6d616d7564'
    }
};

fetch(
    'https://api.cartolafc.globo.com/auth/liga/letnis-champions-league',
    requestInit
).then(
    async (response: Response) => console.log(await response.json()),
    (error) => console.log(error)
);